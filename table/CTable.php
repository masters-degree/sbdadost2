<?php

class CTable {

    private $table;

    function __construct() {
        include_once ROOT . DS. 'table'. DS .'MTable.php';
        $this->table = new MTable();
    }

    /**
     * Fator de bloco
     * fR = tbloco / tR (arredonda pra baixo)
     */
    public function getBlockFactor() {
        return floor($this->table->getTblock() / $this->table->getTR());
    }

    /**
     * Cardinalidade (estimada) do atributo ai
     * CR(ai) = nR / VR(ai)
     */
    public function getCardinality() {
        return $this->table->getNR() / $this->table->getVRai();
    }

    /**
     * Número de blocos necessários para manter tuplas de R
     * bR = nR / fR (arredonda pra cima)
     */
    public function getNumberOfBlocks() {
        return ceil($this->table->getNR() / $this->getBlockFactor());
    }

    /**
     * Busca linear
     * Se chave, custo = (bR / 2) ou
     * não chave: custo = (bR)
     */
    public function getLinearSearchCost() {
        if ($this->table->getFlagAi()) {
            return $this->getNumberOfBlocks() / 2;
        } else {
            return $this->getNumberOfBlocks();
        }
    }

    /**
     * Busca binária
     * Se chave, custo = log2 bR + (CR(ai) / fR ) – 1 (arredonda pra cima) e (arredonda pra cima)
     * não chave, custo = log2 bR (arredonda pra cima)
     */
    public function getBinarySearchCost() {
        if ($this->table->getFlagAi()) {
            return ceil(log($this->getNumberOfBlocks(), 2));
        } else {
            return ceil(log($this->getNumberOfBlocks(), 2)) + ceil(($this->getCardinality() / $this->getBlockFactor())) - 1;
        }
    }

    /**
     * hi: número de níveis (de blocos) do índice para valores de um atributo ai
     * hi = log fi (VR(ai) / N) (arredonda pra cima) e (arredonda pra cima)
     */
    public function getIndexLevelAmount() {
      //echo "Hi:: VR:(".$this->table->getVRai().")/" . "(".$this->table->getN().")=". $this->table->getVRai() / $this->table->getN();
      //echo "<br/>Ceil: " . ceil($this->table->getVRai() / $this->table->getN());
      //echo "<br/>Log: base fi(" . $this->table->getFi(). ")=" . log(ceil($this->table->getVRai() / $this->table->getN()), $this->table->getFi());
        return ceil(log(ceil($this->table->getVRai() / $this->table->getN()), $this->table->getFi()));
    }

    /**
     * Índice primário
     * Se chave, custo = hi + 1
     * não chave, custo = hi + (CR(ai) / fR) (arredonda pra cima)
     */
    public function getIndexedSearchPrimary() {
        if ($this->table->getFlagAi()) {
            return $this->getIndexLevelAmount() + 1;
        } else {
            return $this->getIndexLevelAmount() + ceil($this->getCardinality() / $this->getBlockFactor());
        }
    }

    /**
     * Índice Secundário
     * Se chave, custo = hi + 1
     * não chave, hi + 1 + CR(ai)
     */
    public function getIndexedSearchSecondary() {
        if ($this->table->getFlagAi()) {
            return $this->getIndexLevelAmount() + 1;
        } else {
            return $this->getIndexLevelAmount() + 1 + $this->getCardinality();
        }
    }

    /**
     * Laço aninhado (nested-loop)
     * custo = bR + bS
     */
    public function getNestedLoop(CTable $tableR, CTable $tableS) {
        return $tableR->getNumberOfBlocks() + $tableR->getNumberOfBlocks() + $tableS->getNumberOfBlocks();
    }

    /**
     * Laço aninhado com índice
     * custo = bR + nR * (hIs + 1)
     */
    public function getIndexedNestedLoop(CTable $tableR, CTable $tableS) {
        return ($tableR->getNumberOfBlocks() + $tableR->getNumberOfBlocks() + ($tableR->table->getNR() * ($tableS->getIndexLevelAmount() + 1)));
    }

    /**
     * Merge-junção
     * custoM-J = bR + bS
     */
    public function getBalancedLine(CTable $tableR, CTable $tableS) {
        return $tableR->getNumberOfBlocks() + $tableR->getNumberOfBlocks() + $tableS->getNumberOfBlocks();
    }

    /**
     * nR = number of tuples
     */
    public function getNR() {
        return $this->table->getNR();
    }

    /**
     * Input data given by the user for a table 'R'
     *
     * $nR; number of tuples
     * $tR; size of each tuple
     * $tblock; size of each block
     * $VRai; number of distinct values of the given attribute ai VR(ai)
     * $flagAi; if the attribute is a key or not
     *
     * Input data given by the user for a index 'i'
     *
     * $fi; block factor of the 'i' index (how many b-three nodes fits in a single block)
     * $N; number of values that fits in a node
     */
    public function start($nR, $tR, $tblock, $VRai, $flagAi, $fi, $N) {
        $this->table->setNR($nR);
        $this->table->setTR($tR);
        $this->table->setTblock($tblock);
        $this->table->setVRai($VRai);
        $this->table->setFlagAi($flagAi);
        if ($fi) {
            $this->table->setFi($fi);
        } else {
            $fi = ceil(($nR * $tR) / $tblock);
        }
        if ($N) {
            $this->table->setN($N);
        } else {
            $this->table->setN($this->getIndexLevelAmount());
        }
    }

}
