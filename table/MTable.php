<?php

class MTable{
  /** Input data given by the user for a table 'R' **/
  private $nR; //number of tuples
  private $tR; //size of each tuple
  private $tblock; //size of each block
  private $VRai; //number of distinct values of the given attribute ai VR(ai)
  private $flagAi; //if the attribute is a key or not
  /** Input data given by the user for a index 'i' **/
  private $fi; //block factor of the 'i' index (how many b-three nodes fits in a single block)
  private $N; //number of values that fits in a node

  function getNR() {
      return $this->nR;
  }

  function getTR() {
      return $this->tR;
  }

  function getTblock() {
      return $this->tblock;
  }

  function getVRai() {
      return $this->VRai;
  }

  function getFlagAi() {
      return $this->flagAi;
  }

  function getFi() {
      return $this->fi;
  }

  function getN() {
      return $this->N;
  }

  function setNR($nR) {
      $this->nR = $nR;
  }

  function setTR($tR) {
      $this->tR = $tR;
  }

  function setTblock($tblock) {
      $this->tblock = $tblock;
  }

  function setVRai($VRai) {
      $this->VRai = $VRai;
  }

  function setFlagAi($flagAi) {
      $this->flagAi = $flagAi;
  }

  function setFi($fi) {
      $this->fi = $fi;
  }

  function setN($N) {
      $this->N = $N;
  }

}
