<?php
/** Input data given by the user for a table 'R' * */
// $nR; number of tuples
// $tR; size of each tuple
// $tblock; size of each block
// $VRai; number of distinct values of the given attribute ai VR(ai)
// $flagAi; if the attribute is a key or not
/** Input data given by the user for a index 'i' * */
// $fi; block factor of the 'i' index (how many b-three nodes fits in a single block)
// $N; number of values that fits in a node

if (isset($_POST['calculate'])) {
    include_once ROOT . DS . 'table' . DS . 'CTable.php';
    $table = new CTable();
    $table->start($_POST['nR'], $_POST['tR'], $_POST['tblock'], $_POST['VRai'], $_POST['flagAi'], $_POST['fi'], $_POST['N']);

    echo "<pre>";
    echo "Fator de bloco: " . $table->getBlockFactor() . "\n";
    echo "Cardinalidade (estimada) do atributo ai: " . $table->getCardinality() . "\n";
    echo "Número de blocos necessários para manter tuplas de R: " . $table->getNumberOfBlocks() . "\n";
    echo "\n";
    echo "Custo da busca linear: " . $table->getLinearSearchCost() . "\n";
    echo "\n";
    echo "Custo da busca binária: " . $table->getBinarySearchCost() . "\n";
    echo "\n";
    echo "Número de níveis (de blocos) do índice para valores de um atributo ai: " . $table->getIndexLevelAmount() . "\n";
    echo "\n";
    echo "Busca indexada, supondo um índice primário em ai: " . $table->getIndexedSearchPrimary() . "\n";
    echo "\n";
    echo "Busca indexada, supondo um índice secundário em ai: " . $table->getIndexedSearchSecondary() . "\n";
    echo '</pre>';
}
?>

<form class="form-horizontal" method="POST">
    <fieldset>
        <!-- Part I -->
        <div
            <div class="alert alert-info col-md-offset-2 col-md-7" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <b>Entradas</b> de dados fornecidas pelo usuário para uma tabela <b>R</b>:
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="appendedtext">Número de tuplas</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="nR" name="nR" class="form-control" placeholder="nR" type="text" value="<?= (isset($_POST['nR'])) ? $_POST['nR'] : ''; ?>">
                        <span class="input-group-addon">nR</span>
                    </div>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="tR">Tamanho de cada tupla</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="tR" name="tR" class="form-control" placeholder="tR" type="text" value="<?= (isset($_POST['tR'])) ? $_POST['tR'] : ''; ?>">
                        <span class="input-group-addon">tR</span>
                    </div>
                    <p class="help-block">em bytes</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="tblock">Tamanho do bloco</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="tblock" name="tblock" class="form-control" placeholder="tblock" type="text" value="<?= (isset($_POST['tblock'])) ? $_POST['tblock'] : ''; ?>">
                        <span class="input-group-addon">tblock</span>
                    </div>
                    <p class="help-block">em bytes</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="VRai">Número de valores distintos</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="VRai" name="VRai" class="form-control" placeholder="Vai" type="text" value="<?= (isset($_POST['VRai'])) ? $_POST['VRai'] : ''; ?>">
                        <span class="input-group-addon">Vai</span>
                    </div>
                    <p class="help-block">de certo atributo ai</p>
                </div>
            </div>
            <!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="flagAi">Tipo de atributo ai</label>
                <div class="col-md-4">
                    <div class="radio">
                        <label for="flagAi-0">
                            <input type="radio" name="flagAi" id="flagAi-0" value="1" <?= (isset($_POST['flagAi']) && $_POST['flagAi']) ? 'checked="checked"' : ''; ?>>
                            Chave
                        </label>
                    </div>
                    <div class="radio">
                        <label for="flagAi-1">
                            <input type="radio" name="flagAi" id="flagAi-1" value="0" <?= (isset($_POST['flagAi']) && $_POST['flagAi']) ? '' : 'checked="checked"'; ?>>
                            Não chave
                        </label>
                    </div>
                </div>
            </div>
            <!-- Part II -->
            <div class="alert alert-info col-md-offset-2 col-md-7" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <b>Entradas</b> de dados fornecidas pelo usuário para um índice <b>i</b>:  
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="appendedtext">Fator de bloco do índice (i)</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="fi" name="fi" class="form-control" placeholder="fi" type="text" value="<?= (isset($_POST['fi'])) ? $_POST['fi'] : ''; ?>">
                        <span class="input-group-addon">fi</span>
                    </div>
                    <p class="help-block">quantos nodos de uma árvore-B cabem em um bloco</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="N">Número de valores que cabem em um nodo</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="N" name="N" class="form-control" placeholder="N" type="text" value="<?= (isset($_POST['N'])) ? $_POST['N'] : ''; ?>">
                        <span class="input-group-addon">N</span>
                    </div>

                </div>
            </div>
            <!-- Button (Double) -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="calculate">Ações</label>
                <div class="col-md-8">
                    <button id="calculate" name="calculate" class="btn btn-success" type="submit">Calcular</button>
                    <button id="reset" name="reset" class="btn btn-warning" type="reset">Limpar</button>
                </div>
            </div>
    </fieldset>
</form>