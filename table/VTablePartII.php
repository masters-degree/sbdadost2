<?php
/**
 * Para calcular este custo, solicite entradas de dados adicionais ao usuário
 *  a respeito da tabela S (decida quais entradas a respeito de S são necessárias).
 *  Não são informados diretamente pelo usuário: número de blocos, fator de bloco e altura
 * da árvore de índices (mas você pode calcular estes valores a partir de outras variáveis de entrada necessárias, como foi feito para a tabela R)
  Mostre os custos parciais dos algoritmos em cada cenário.
  Ex:  custo total cenário 1: 17 (10 para projeção e 7 para a junção com laço aninhado)
 */
if (isset($_POST['calculate'])) {
    include_once ROOT . DS . 'table' . DS . 'CTable.php';
    $table = new CTable();

    $tableR = new CTable();
    $tableR->start($_POST['r-nR'], $_POST['r-tR'], $_POST['r-tblock'], $_POST['r-VRai'], $_POST['r-flagAi'], $_POST['r-fi'], $_POST['r-N']);
    $tableS = new CTable();
    $tableS->start($_POST['s-nR'], $_POST['s-tR'], $_POST['s-tblock'], $_POST['s-VRai'], $_POST['s-flagAi'], $_POST['s-fi'], $_POST['s-N']);

    echo "<pre>";
    echo "Laço aninhado no melhor caso : " . $table->getNestedLoop($tableR, $tableS) . ' (' . $tableR->getNumberOfBlocks() . ' + ' . $tableR->getNumberOfBlocks() . ' + ' . $tableS->getNumberOfBlocks() . ')' . "\n";
    echo "\n";
    echo "Laço aninhado com índice: " . $table->getIndexedNestedLoop($tableR, $tableS) . ' (' . $tableR->getNumberOfBlocks() . ' + ' .$tableR->getNumberOfBlocks() . ' + ' . $tableR->getNR() . ' * (' . $tableS->getIndexLevelAmount() . ' + 1))' . "\n";
    echo "\n";
    echo "Merge junção (desconsidere o custo da ordenação): " . $table->getBalancedLine($tableR, $tableS) . ' (' .$tableR->getNumberOfBlocks() . ' + ' . $tableR->getNumberOfBlocks() . ' + ' . $tableS->getNumberOfBlocks() . ')' . "\n";
    echo '</pre>';
}
?>
<!-- Part II -->
<form class="form-horizontal" method="POST">
    <fieldset>
        <!--Table R -->
        <div class="col-md-6">
            <div class="alert alert-info col-md-offset-2 col-md-7" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <b>Entradas</b> de dados fornecidas pelo usuário para uma tabela <b>R</b>:
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="r-nR">Número de tuplas</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="r-nR" name="r-nR" class="form-control" placeholder="r-nR" type="text" value="<?= (isset($_POST['r-nR'])) ? $_POST['r-nR'] : ''; ?>">
                        <span class="input-group-addon">nR</span>
                    </div>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="r-tR">Tamanho de cada tupla</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="r-tR" name="r-tR" class="form-control" placeholder="r-tR" type="text" value="<?= (isset($_POST['r-tR'])) ? $_POST['r-tR'] : ''; ?>">
                        <span class="input-group-addon">tR</span>
                    </div>
                    <p class="help-block">em bytes</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="r-tblock">Tamanho do bloco</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="r-tblock" name="r-tblock" class="form-control" placeholder="r-tblock" type="text" value="<?= (isset($_POST['r-tblock'])) ? $_POST['r-tblock'] : ''; ?>">
                        <span class="input-group-addon">tblock</span>
                    </div>
                    <p class="help-block">em bytes</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="r-VRai">Número de valores distintos</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="r-VRai" name="r-VRai" class="form-control" placeholder="r-Vai" type="text" value="<?= (isset($_POST['r-VRai'])) ? $_POST['r-VRai'] : ''; ?>">
                        <span class="input-group-addon">Vai</span>
                    </div>
                    <p class="help-block">de certo atributo ai</p>
                </div>
            </div>
            <!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="r-flagAi">Tipo de atributo ai</label>
                <div class="col-md-4">
                    <div class="radio">
                        <label for="r-flagAi-0">
                            <input type="radio" name="r-flagAi" id="r-flagAi-0" value="1" <?= (isset($_POST['r-flagAi']) && $_POST['r-flagAi']) ? 'checked="checked"' : ''; ?>>
                            Chave
                        </label>
                    </div>
                    <div class="radio">
                        <label for="r-flagAi-1">
                            <input type="radio" name="r-flagAi" id="r-flagAi-1" value="0" <?= (isset($_POST['r-flagAi']) && $_POST['r-flagAi']) ? '' : 'checked="checked"'; ?>>
                            Não chave
                        </label>
                    </div>
                </div>
            </div>
            <!-- Part II -->
            <div class="alert alert-info col-md-offset-2 col-md-7" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <b>Entradas</b> de dados fornecidas pelo usuário para um índice <b>i</b>:
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="r-fi">Fator de bloco do índice (i)</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="r-fi" name="r-fi" class="form-control" placeholder="r-fi" type="text" value="<?= (isset($_POST['r-fi'])) ? $_POST['r-fi'] : ''; ?>">
                        <span class="input-group-addon">fi</span>
                    </div>
                    <p class="help-block">quantos nodos de uma árvore-B cabem em um bloco</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="r-N">Número de valores que cabem em um nodo</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="r-N" name="r-N" class="form-control" placeholder="r-N" type="text" value="<?= (isset($_POST['r-N'])) ? $_POST['r-N'] : ''; ?>">
                        <span class="input-group-addon">N</span>
                    </div>

                </div>
            </div>
        </div>
        <!--Table S -->
        <div class="col-md-6">
            <div class="alert alert-info col-md-offset-2 col-md-7" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <b>Entradas</b> de dados fornecidas pelo usuário para uma tabela <b>S</b>:
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="s-nR">Número de tuplas</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="s-nR" name="s-nR" class="form-control" placeholder="s-nR" type="text" value="<?= (isset($_POST['s-nR'])) ? $_POST['s-nR'] : ''; ?>">
                        <span class="input-group-addon">nR</span>
                    </div>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="tR">Tamanho de cada tupla</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="s-tR" name="s-tR" class="form-control" placeholder="s-tR" type="text" value="<?= (isset($_POST['s-tR'])) ? $_POST['s-tR'] : ''; ?>">
                        <span class="input-group-addon">tR</span>
                    </div>
                    <p class="help-block">em bytes</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="tblock">Tamanho do bloco</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="s-tblock" name="s-tblock" class="form-control" placeholder="s-tblock" type="text" value="<?= (isset($_POST['s-tblock'])) ? $_POST['s-tblock'] : ''; ?>">
                        <span class="input-group-addon">tblock</span>
                    </div>
                    <p class="help-block">em bytes</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="s-VRai">Número de valores distintos</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="s-VRai" name="s-VRai" class="form-control" placeholder="s-VRai" type="text" value="<?= (isset($_POST['s-VRai'])) ? $_POST['s-VRai'] : ''; ?>">
                        <span class="input-group-addon">Vai</span>
                    </div>
                    <p class="help-block">de certo atributo ai</p>
                </div>
            </div>
            <!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="s-flagAi">Tipo de atributo ai</label>
                <div class="col-md-4">
                    <div class="radio">
                        <label for="s-flagAi-0">
                            <input type="radio" name="s-flagAi" id="s-flagAi-0" value="1" <?= (isset($_POST['s-flagAi']) && $_POST['s-flagAi']) ? 'checked="checked"' : ''; ?>>
                            Chave
                        </label>
                    </div>
                    <div class="radio">
                        <label for="s-flagAi-1">
                            <input type="radio" name="s-flagAi" id="s-flagAi-1" value="0" <?= (isset($_POST['s-flagAi']) && $_POST['s-flagAi']) ? '' : 'checked="checked"'; ?>>
                            Não chave
                        </label>
                    </div>
                </div>

            </div>
            <!-- Part II -->
            <div class="alert alert-info col-md-offset-2 col-md-7" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <b>Entradas</b> de dados fornecidas pelo usuário para um índice <b>i</b>:
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="s-fi">Fator de bloco do índice (i)</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="s-fi" name="s-fi" class="form-control" placeholder="s-fi" type="text" value="<?= (isset($_POST['s-fi'])) ? $_POST['s-fi'] : ''; ?>">
                        <span class="input-group-addon">fi</span>
                    </div>
                    <p class="help-block">quantos nodos de uma árvore-B cabem em um bloco</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="s-N">Número de valores que cabem em um nodo</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="s-N" name="s-N" class="form-control" placeholder="s-N" type="text" value="<?= (isset($_POST['s-N'])) ? $_POST['s-N'] : ''; ?>">
                        <span class="input-group-addon">N</span>
                    </div>

                </div>
            </div>

        </div>
        <!-- Button (Double) -->
        <br/>
        <br/>
        <br/>
        <div class="form-group col-md-12">
            <label class="col-md-4 control-label" for="calculate">Ações</label>
            <div class="col-md-8">
                <button id="calculate" name="calculate" class="btn btn-success" type="submit">Calcular</button>
                <button id="reset" name="reset" class="btn btn-warning" type="reset">Limpar</button>
            </div>
        </div>
    </fieldset>
</form>
