This is a project about functions to calculate costs of query processing algorithms. It's related to the subject of Database System, Masters Degree in Computing Science. Federal University of Santa Maria - Brazil. The description of the implementation problem is included (password mat20161).

Developed by Douglas Haubert <dhaubert.ti@gmail.com> and Tobias Senger <tsenger.eti@gmail.com>.
